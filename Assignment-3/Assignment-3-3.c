#include<stdio.h>

int main()
{	
	int number, reverseNo = 0, remainder = 0;
	
	printf("Enter an integer: ");
	scanf("%d", &number);
	
	while(number!=0)
	{
		remainder = number%10;
		reverseNo = reverseNo*10 + remainder;
		number/=10;
	}
	
	printf("Reversed number : %d", reverseNo);

	return 0;	
}

