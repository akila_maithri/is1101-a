#include<stdio.h>

int main()
{
	int number, tableNo, x;
	
	printf("Enter the number of tables: ");
	scanf("%d", &number);
		
	for(tableNo=1; tableNo<=number; tableNo++)
	{
		printf("Multiplication table of %d\n", tableNo);
		
		for(x=1; x<=12; x++)
		{
			printf("%d X %d = %d\n", tableNo, x, tableNo*x) ;
		}
		
		printf("\n");
	}
	
	return 0;
}

