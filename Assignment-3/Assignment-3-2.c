#include<stdio.h>

int main()
{
	int number, divider=2;
	
	printf("Enter a positive integer: ");
	scanf("%d", &number);
	
	while(number%divider !=0) divider++;
	
	if(number==divider)printf("\n %d is a prime number.", number);
	else printf("\n %d is not a prime number.", number);
	
	return 0;
}

