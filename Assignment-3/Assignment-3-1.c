#include<stdio.h>

int main()
{
	int number, sum=0;
	
	do
	{
		printf("Enter positive numbers to find sum: ");
		scanf("%d", &number);
		
		if(number<=0)break;
		
		sum+=number;
			
	}while(number>0);
	
	printf("\nSum of numbers entered: %d\n", sum);
	
	return 0;
}
