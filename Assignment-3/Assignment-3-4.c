#include<stdio.h>

int main()
{
	int number, divider=1;
	
	printf("Enter a number : ");
	scanf("%d", &number);
	
	printf("Factors are: ");
	
	while(divider<=number)
	{
		if(number%divider == 0) printf("%d ", divider);
		divider++;
	}
	
	printf("\n");

	return 0;	
}
