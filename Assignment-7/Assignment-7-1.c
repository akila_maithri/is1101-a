/*S.A.A.D.Maithripala - 19020432
1. Write a C program to reverse a sentence entered by user.*/

#include<stdio.h>
#define arrSize 100	                        

int main()
{
    int i;
    char arrSentence[arrSize] = {'\0'};		//added null terminator '\0'

    printf("Enter sentence : ");
    fgets(arrSentence, sizeof(arrSentence), stdin);
    
    for(i=sizeof(arrSentence)-1; i>=0; i--)	
    {
        printf("%c", arrSentence[i]);
    }

    return 0;
}
