/*S.A.A.D.Maithripala - 19020432
3. Write a program to add and multiply two given matrices.

Addition method used in this code can process matrices of different row/column combinations, but outputs the answer in the 
common row/column combination for both matrices. 
eg: 4x3 matrix + 2x3 matrix = 2x3 matrix */

#include <stdio.h>

//maximum rows/columns per matrix.
#define max 10  

//function to input elements to the matrix array.
void matricesElements(int matA[max][max], int matB[max][max], int rowsA, int colsA, int rowsB, int colsB);

//function to add two matrices.
void matricesAdder(int marA[max][max], int matB[max][max], int matSum[max][max], int rowsA, int colsA, int rowsB, int colsB);

//function to multiply two matrices.
void matricesMultiplier(int matA[max][max], int matB[max][max], int matMul[max][max], int rowsA, int colsA, int rowsB, int colsB);

int main() 
{
    int rowsA, colsA, rowsB, colsB;
    int matA[max][max], matB[max][max], matSum[max][max], matMul[max][max];
    
    printf("\nMaxium Row/Column size is %d.\n\n",max);

    printf("Enter the number of rows in the 1st matrix\t:");
    scanf("%d", &rowsA);
    printf("Enter the number of columns in the 1st matrix\t:");
    scanf("%d", &colsA);
        
    printf("\nEnter the number of rows in the 2nd matrix\t:");  
    scanf("%d", &rowsB);

    printf("Enter the number of columns in the 2nd matrix\t:");
    scanf("%d", &colsB);

    if(colsA>rowsB)rowsB=colsA;
    else if(colsA<rowsB) colsA=rowsB;

        printf("\n\t This program is made to calculate both addition and multiplication of two matrices at once.");
        printf("\n\t As the no. of 'Columns in Matrix-1' has to be equal to the no. of 'Rows in Matrix-2' to multiply correctly,");
        printf("\n\t if two values are different, highest value is assigned to both of them.\n");

        printf("\nMatrix 1 = %d-Rows x %d-Columns", rowsA, colsA);
        printf("\nMatrix 2 = %d-Rows x %d-Columns\n", rowsB, colsB);

        printf("\nRead the below R-values and C-values carefully!.\n");
        printf("If you are asked to enter Row-Column combinations beyond the elements of your matrix, enter 0.\n");

    matricesElements(matA,matB,rowsA,colsA,rowsB,colsB);

    matricesAdder(matA,matB,matSum,rowsA,colsA,rowsB,colsB);
    matricesMultiplier(matA,matB,matMul,rowsA,colsA,rowsB,colsB);

    return 0;
}
    

void matricesElements(int matA[max][max], int matB[max][max], int rowsA, int colsA, int rowsB, int colsB)
{
    int i,j;
    printf("\nEnter elements of Matrix 1:\n");
    for(i=0; i<rowsA; i++) 
    {
        for (j=0; j<colsA; j++) 
        {
            printf("Enter R%d-C%d: ", i+1, j+1);
            scanf("%d", &matA[i][j]);
        }
    }

    printf("\nEnter elements of Matrix 2:\n");
    for(i=0; i<rowsB; i++) 
    {
        for(j=0; j<colsB; j++) 
        {
            printf("Enter R%d-C%d: ", i+1, j+1);
            scanf("%d", &matB[i][j]);
        }
    }
}

void matricesAdder(int matA[max][max], int matB[max][max], int matSum[max][max], int rowsA, int colsA, int rowsB, int colsB)
{
    int i,j, rows, cols;

    if(rowsA<=rowsB) rows = rowsA;
    else rows = rowsB;

    if(colsA<=colsB) cols = colsA;
    else cols=colsB;

    //resetting the elements of Summation Matrix array to 0.
    for(i=0; i<rows; i++)
    {
        for(j=0; j<cols; j++)
        {
            matSum[i][j] = 0;
        }
    }

    //adding Matrix A and Matrix B. Storing the answer in matSum array.
    for(i=0; i<rows; i++)
    {
        for(j=0; j<cols; j++)
        {
            matSum[i][j] = matA[i][j] + matB[i][j];
        }
    }

    //printing the results from matSum array. 
    printf("\n\n{Matrix A} + {Matrix B}\n");
    for(i=0; i<rows; i++)
    {
        for(j=0; j<cols; j++)
        {
            printf("%d\t", matSum[i][j]);
            if(j == cols-1) printf("\n\n");
        }
    }
}

void matricesMultiplier(int matA[max][max], int matB[max][max], int matMul[max][max], int rowsA, int colsA, int rowsB, int colsB)
{
    int i,j,k;

    //resetting the elements of Multiplication Matrix array to 0.
    for(i=0; i<rowsA; i++) 
    {
        for(j=0; j<colsB; j++) 
        {
            matMul[i][j] = 0;
        }
    }

    //multiplying Matrix A and Matrix B. Storing the answers in matMul array.
    for(i=0; i<rowsA; i++) 
    {
        for(j=0; j<colsB; j++) 
        {
            for(k=0; k<colsA; k++) 
            {
                matMul[i][j] += matA[i][k] * matB[k][j];
            }
        }
    }

    //printing the result from matMul array.
    printf("\n\n{Matrix A} X {Matrix B}\n");
    for(i=0; i<rowsA; i++) 
    {
        for(j=0; j<colsB; j++) 
        {
            printf("%d\t", matMul[i][j]);
            if(j == colsB-1)
                printf("\n\n");
        }
    }
}
