/*S.A.A.D.Maithripala - 19020432
2. Write a program to find the Frequency of a given character in a given string.*/

#include<stdio.h>
#define arrSize 50                                  

int main()
{  
    int i, count=0;
    char character, arrString[arrSize] = {'\0'};    //added null terminator '\0' to array arrString.

    printf("Enter the string : ");
    fgets(arrString, sizeof(arrString), stdin);

    printf("Enter the character you want to know the frequency of : ");
    scanf("%c", &character);

    for(i=0; arrString[i] != '\0'; i++)
    {
        if(character == arrString[i]) count++;
    }
    
    printf("\nFrequency of '%c' is %d.", character, count);

    return 0;   
}