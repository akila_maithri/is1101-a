//19020432 - S.A.A.D.Maithripala - Assignment-8

#include<stdio.h>

#define stuCount 5

//declaring structure
struct stuDetails
{
    char stuName[10];
    char subName[10];
    int marks;
}stuArray[stuCount];

int main()
{
    int i;

    //get the data to the array structure
    for(i=0; i<stuCount; i++)
    {
        printf("\nStudent Name : ");
        scanf("%s", stuArray[i].stuName);
        
        printf("Subject Name : ");
        scanf("%s", stuArray[i].subName);

        printf("Marks : ");
        scanf("%d", &stuArray[i].marks);

    }

    //print data in the array structure
    for(i=0; i<stuCount; i++)
    {
        printf("\n\nRecord of Student %d\n", i+1);

        printf("\nName : %s", stuArray[i].stuName);
        printf("\nSubject : %s", stuArray[i].subName);
        printf("\nMarks : %d", stuArray[i].marks);
    }

    return 0;
}
