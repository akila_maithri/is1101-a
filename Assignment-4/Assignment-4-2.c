#include<stdio.h>

int main()
{
    const double pi = 3.14;
    float height, radius, volume;

    printf("Enter height and radius of cone seperated by spaces: ");
    scanf("%f %f", &height, &radius);

    volume = pi*pow(radius,2)*height/3;

    printf("Volume of the cone = %.3f", volume);

    return 0;
}