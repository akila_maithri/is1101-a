#include <stdio.h>

#define performanceCost 500
#define perAttendee 3

//functions used
int attendees(int price);    //calculates expected no. of attendees for the given ticket price
int revenue(int price);      //calculates the revenue from attendees
int cost(int price);         //calcualtes total cost for one show
int profit(int price);       //calcualtes net profit from one show

int attendees(int price)    
{
    return (120-(price-15)/5*20);
}

int revenue(int price)     
{
    return price*attendees(price);
}

int cost(int price)        
{
    return performanceCost + attendees(price)*perAttendee;
}

int profit(int price)
{
    return revenue(price)-cost(price);
}

int main()
{
    printf("Expected profit for Ticket price (Rs.5 to Rs.45)\n\n");

    printf("Ticket Price \t Profit\n");

    int ticketPrice;
	for(ticketPrice = 5 ; ticketPrice<50; ticketPrice+=5)
    {
        printf("Rs.%d \t\t Rs.%d\n", ticketPrice, profit(ticketPrice));
    }

    return 0;
}

