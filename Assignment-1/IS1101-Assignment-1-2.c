#include<stdio.h>

int main()
{
	const double pi = 3.14;
	float radius, area;
	
	printf("Enter radius of the disk : ");
	scanf("%f", &radius);
	
	area= pi*pow(radius,2);	;
	
	printf("Area of the disk is %.2f sqr units", area);
	
	return 0;
}

