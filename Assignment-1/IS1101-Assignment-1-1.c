#include<stdio.h>

int main()
{
	float n1, n2, answer;
	
	printf("Enter two numbers to multiply (eg: number1 number2) : ");
	scanf("%f %f", &n1, &n2);
	
	answer = n1*n2;
	
	printf("Answer is %.2f", answer);
	
	return 0;
}
