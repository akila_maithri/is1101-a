#include<stdio.h>

int main()
{
	int n1, n2, x;
	
	printf("Enter two integers to swap (eg: Number1 Number2) : ");
	scanf("%d %d", &n1, &n2);
	
	printf("Number1 is %d and Number2 is %d\n	Swapping...\n", n1, n2);
	
	x=n1;
	n1=n2;
	n2=x;
	
	printf("Number1 is %d and Number2 is %d", n1, n2);
	
	return 0;
}
	
