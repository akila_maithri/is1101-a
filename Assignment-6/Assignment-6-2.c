/* S.A.A.D.Maithriala - 202043 | 2019/IS/043 
Write a recursive function fibonacciSeq(n) to print the fibonacci sequence up to n.*/

#include<stdio.h>

//functions used
int fibonacciSeq(int input);	//gets the input (n) for fibonacci Seq. and prints the subseq. numbers upto that.
int fibonacci(int n);			//returns the subsequent number for current n value. 		

int userInput, number=0, counter=0;

int fibonacci(int n)
{
	if(n==0) 
		return 0;
	else if(n==1) 
		return 1;
	else 
		return (fibonacci(n-1) + fibonacci(n-2));
}

int fibonacciSeq(int input)
{
	if(counter<=input)
	{
		printf("%d\n",fibonacci(number));
		number++;
		counter++;
		fibonacciSeq(input);
	}
	else return 0;
}

int main()
{
	printf("Enter number : ");
	scanf("%d", &userInput);

	fibonacciSeq(userInput);
}