/* S.A.A.D.Maithriala - 202043 | 2019/IS/043 
Write a recursive function pattern(n) which prints a number triangle. */

#include<stdio.h>

//functions used
void pattern(int n);	//gets the input and controls the pattern accordingly.
void patternRow(int r);	//prints a single row according to the current number.

int rowNo=1, input;

void pattern(int n)	
{
	if(n>0)
	{ 
		patternRow(rowNo);
		printf("\n");
		rowNo++;
		pattern(n-1);
	}
}

void patternRow(int r)
{
	if (r>0) 
	{
		printf("%d ", r);
		patternRow(r-1);
	}
}

int main()
{
	printf("Enter no. of lines: ");
	scanf("%d", &input);
	pattern(input);
    
    return 0;
}